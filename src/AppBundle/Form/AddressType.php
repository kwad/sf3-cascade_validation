<?php

namespace AppBundle\Form;

use AppBundle\Model\Address;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('zipCode', TextType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
//            'validation_groups' => function(FormInterface $form) {
//                /** @var Address $data */
//                $data              = $form->getData();
//                $validation_groups = [];
//
//                // Simplified here, it's a service call with heavy logic
//                if ($data->doesRequireZip()) {
//                    $validation_groups[] = 'zipRequired';
//                }
//
//                return $validation_groups;
//            }
        ]);
    }
}
<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Model\Address;
use AppBundle\Model\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $user = new User();

        $address1 = new Address();
        $address1->setZipCode('49000');

        $address2 = new Address();
        $address2->setZipCode('37000');


        $user->setAddresses($address1);
        $user->setAddresses($address2);

        $form = $this->createForm(UserType::class);

        $form->handleRequest($request);

        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
